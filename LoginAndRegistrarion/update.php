<?php
require_once 'core/init.php';

$user = new User();
if(!$user->isLoggedIn())
{
    Redirect::to('index1.php');
}

if(Input::exists())
{
    if(Token::check(Input::get('token')))
    {
        $validate = new Validate();
        $validation= $validate->check($_POST, array(
            'name' => array(
                'required' => true,
                'min' => 3,
                'max'=> 20
            )


        ));
        if($validation->passed())
        {
            try
            {
                //update the user information
                $user->update(array(
                    'name' => Input::get('name')
                ));
                Session::flash('home', 'yor details have been updated..');
                Redirect::to('index1.php');
            }

            catch(Exception $e)
            {
                die($e->getMessage());
            }
        }
        else
        {

            foreach ($validation->errors() as $error)
            {
                echo '<br>'.$error;

            }
        }
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Update Data</title>
</head>
<body>
<form action="" method="post">

    <div class="field">
        <label for="name">Enter Name</label>
        <input type="text" name="name" id="name" autocomplete="off" value="<?php echo escape($user->data()->name); ?> ">

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
        <input type="submit" name="submit" value="UPDATE">
    </div>
</form>

</body>
</html>
