<?php
require_once 'core/init.php';
//var_dump(Token::check(Input::get('token')));

if (Input::exists()){
    if (Token::check(Input::get('token'))){   // this is used for csrf protrction i.e token maching with md5 unique id
        echo 'CSRF protection is running'.'<br/>';
        $validate=new Validate();
        $validatetion=$validate->check($_POST,array(
            'username'=>array(
                'required'=>true,
                'min'=>3,
                'max'=>20,
                'unique'=>'user'
            ),
            'password'=>array(
                'required'=>true,
                'min'=>6

            ),
            'password_again'=>array(
                'required'=>true,
                'matches'=> 'password'
            ),
            'name'=>array(
                'required'=>true,
                'min'=>3
            ),
        ));
        if ($validatetion->passed()){
                $user=new User();                         //  echo 'passed'; //Register User
               echo $salt=Hash::salt(32);
            try{
                 $user->create(array(
                         'username' => Input::get('username'),
                         'password' => Hash::make(Input::get('password'),$salt  ),
                         'salt' =>$salt,
                         'name' =>Input::get('name'),
                         'joined' => date('Y-m-d H:i:s'),
                         'group' => 1
                 ));
                 Session::flash('home','You Have been registered');
                // header('Location: index1.php');
                Redirect::to('login.php');

            }catch (Exception $e){
                echo 'catch';
                die($e->getMessage());

            }
        }else{
            foreach ($validatetion->errors() as $error){
                echo $error.'<br/>';
            }//ERROR
        }
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<br>

<div class="form-group">
    <form action="" method="post" >
        <div class="field">
            <label for="username">UserName</label>
            <input type="text" class="form-control" name="username" id="username" value="<?php echo escape(Input::get('username'))?>" autocomplete="off">

        </div>
        <div class="field">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" value="">

        </div>

        <div class="field">
            <label for="password_again">Password Again</label>
            <input type="password" class="form-control" name="password_again" id="password_again" value="">

        </div>
        <div class="field">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" id="name" value="<?php echo escape(Input::get('name'))?>">

        </div>
        <br>
        <input type="hidden" name="token" value="<?php echo Token::generate()?>">
        <input type="submit" value="Register" class="btn btn-success center-block">
    </form>
</div>

</body>
</html>