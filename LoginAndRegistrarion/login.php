<?php
require_once 'core/init.php';

if (Input::exists()){
   if (Token::check(Input::get('token'))){

       $validate= new Validate();
       $validation= $validate->check($_POST,array(
               'username' => array('required' => true),
               'password' => array('required' => true)
       ));

       if ($validation->passed()){
         $user= new User();
         $remember = (Input::get('remember') === 'on') ? true : false;
         $login= $user->login(Input::get('username'),Input::get('password'),$remember);

         if ($login){
           Redirect::to('index1.php');
         }else{
             echo'Login Failed';
         }

       }else{
           foreach ($validation->errors() as $error){
               echo $error . '<br/>';
           }
       }
   }
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<div class="container ">
    <div class="row ">
        <form action="#" method="post">
            <div class="col-xs-3"></div>
            <div class="col-xs-6  jumbotron">
                <div class="row well form-group">
                    <h2 class="text-center">Welcome!</h2>
                    <label for="username">User Name</label>
                    <input type="text" autocomplete="off" name="username" class="form-control" id="username" >
                    <label for="password">Password</label>
                    <input type="text" autocomplete="off" name="password" class="form-control" id="password" ><br>
                    <label for="remember"> <input type="checkbox" name="remember" id="remember" > Remember me</label>
                    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                    <button class="btn btn-success" type="submit">Sign In</button>

                    <a href="#" class="pull-right">Register</a>

                </div>


                <hr>

            </div>
            <div class="col-xs-3"></div>
        </form>
    </div>
</div>

</body>
</html>


