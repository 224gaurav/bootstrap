<?php
require_once 'core/init.php';

$user = new User();

if (!$user->isLoggedIn()){
    Redirect::to('index1.php');
}
if (Input::exists()){
    if (Token::check(Input::get('token'))){
      $validate= new Validate();
      $validation= $validate->check($_POST, array(
          'password_current' => array(
              'required' => true,
              'min' => 6
          ),
          'password_new' => array(
              'required' => true,
              'min' => 6
          ),
          'password_new_again' => array(
              'required' => true,
              'min' => 6,
              'matches' => 'password_new'
          )

      ));

      if($validation->passed()){
         if (Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password){
              echo 'current password is wrong';
         } else{
            $salt = Hash::salt(32);
            $user->update(array(
                    'password' => Hash::make(Input::get('password_new'), $salt),
                     'salt' => $salt
            ));
            Session::flash('home', 'Your password has been changed');
            Redirect::to('index1.php');
         }
      }else{
          foreach ($validation->errors() as $error){
              echo $error . '<br/>';
          }
      }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<br>

<div class="form-group">
    <form action="" method="post" >
        <div class="field">
            <label for="password_current">Password</label>
            <input type="text" class="form-control" name="password_current" id="password_current" autocomplete="off">

        </div>
        <div class="field">
            <label for="password_new">Password new</label>
            <input type="password" class="form-control" name="password_new" id="password_new" >

        </div>

        <div class="field">
            <label for="password_new_again">Password new</label>
            <input type="password" class="form-control" name="password_new_again" id="password_new_again" >

        </div>

        <br>
        <input type="hidden" name="token" value="<?php echo Token::generate()?>">
        <input type="submit" value="Change" class="btn btn-success center-block">
    </form>
</div>

</body>
</html>
